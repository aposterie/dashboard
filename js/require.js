/* require.js
 * Copyright (C) 2015 Alex Cai <t9cai@uwaterloo.ca>
 * Released under MIT License.
 */
 
/* Interface 
 * 
 * Added Functions:
 * ================================================================================
 * require(file1, file2, ...) appends a JavaScript or stylesheet element in <head>.
 * require(file1, type) appends a JavaScript (if type == 'js') or a stylesheet 
 *   (if type == 'css') in <head>.
 * remove(file) removes a JavaScript or stylesheet from <head>.
 *
 * Note:
 * ================================================================================
 *    require() will not append the given file if it is already injected through
 * require() before.
 */

(function (document, undefined) {
   var required = [];
   
   var requireOne = function (file, type) {
      if (required.indexOf(file) > -1) {
         return;
      } else if (type && type === 'css' || file.slice(-4) === '.css') {
         var e = document.createElement("link");
         e.rel = "stylesheet"
         e.href = file;
         document.head.appendChild(e);
         required.push(file);
      } else if (type && type === 'js' || file.slice('-3') === '.js') {
         var e = document.createElement("script");
         e.src = file;
         document.head.appendChild(e);
         required.push(file);
      }
   };

   var require = function () {
      if (arguments.length == 2 && ['css', 'js'].indexOf(arguments[1]) > -1) {
         requireOne.call(this, arguments);
      } else {
         Array.prototype.forEach.call(arguments, requireOne);
      }
   };
   
   var remove = function (file) {
      if (file && file.length) {
         file = file.replace('"', '\\"');
         var element = document.querySelector('[src="' + file + '"], [href="' + file + '"]');
         element.outerHTML = "";
         delete element;
         if (required.indexOf(file) > -1) {
            required.splice(required.indexOf(file), 1);
         }
      }
   };

   window.require = require;
   window.remove = remove;
})(document);
