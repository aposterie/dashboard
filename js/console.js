var format = function (thing, custom) {
   if (custom) {
      return '<span class="cm-' + custom + '">' + thing + '</span>';
   }
   switch (getTypeOf(thing)) {
      case 'number': 
         return '<span class="cm-number">' + escapeHTML(thing) + '</span>';
      case 'string':
         return '<span class="cm-string">"' + escapeHTML(thing) + '"</span>';
   }
};

var escapeHTML = function (str) {
   return str.toString().replace('<', '&lt;');
};

var getIcon = function (name) {
   return '<span class="glyphicon glyphicon-' + name + '" aria-hidden="true"></span>';
};

var stringify = function (obj) {
   switch (getTypeOf(obj)) {
      case 'undefined':
         return format(obj, 'hr');
      // case 'number':
      case 'string':
         return format(obj);
      case 'array':
         return "[" + obj.map(function (e){return format(e);}).join(', ') + "]";
      case 'function':
         return obj.toString();
      case 'boolean':
         return format(obj.toString(), 'atom');
      case 'object':
         if (typeof obj === 'string') return obj;

         var counter = 0, result = '{';
         for (key in obj) {
            if (counter > 5) {
               result += '...}';
               break;
            } else if (obj.hasOwnProperty(key)) {
               if (counter) result += ', ';
               counter++;
               result += format(key, "keyword") + ": " + obj[key].toString();
            }
         }
         return result;
      default:
         return obj.toString();
   }
};

var jsConsoleHistory = [];

var addConsoleResult = function (content) {
   $('#console-result').html(content + "<hr>" + $('#console-result').html());
}

// JavaScript Console
var jsConsole = CodeMirror.fromTextArea(document.querySelector('#console textarea'), {
   autofocus: true,
   placeholder: 'Enter JavaScript code here...',
   extraKeys: {
      "Enter": function (cm) {
         var statement = '', result = undefined;
         try {
            result = window.eval(cm.getValue());
         } catch (exception) {
            if (exception instanceof SyntaxError) {
               statement = format("Syntax Error.", 'error');
            } else if (exception instanceof RangeError) {
               statement = format("Range Error.", 'error');
            } else if (exception instanceof TypeError) {
               statement = format("Type Error.", 'error');
            }
         } finally {
            if (!statement) {
               var statement = stringify(result);
            }
            jsConsoleHistory.push(cm.getValue());
            var prefix = '<div class="input">' + getIcon('chevron-right') + format(cm.getValue().replace('\n', '<br>'), 'attribute') + '</div>';
            var postfix = getIcon('chevron-left') + statement;

            addConsoleResult(prefix + postfix);
            cm.setValue('');
         }
      },
      "Up": function (cm) {
         if (jsConsoleHistory.length) {
            cm.setValue(jsConsoleHistory.pop());
         }
      }
   }
});

// Overriding console.log
(function(){
    var oldLog = console.log;
    window.console.log = function (message) {
        addConsoleResult(message);
        try {
         oldLog.apply(console, arguments);
        } finally {
         // never mind;
        }
    };
})();