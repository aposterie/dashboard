// JavaScript Playground
var htmlPlayground = CodeMirror.fromTextArea(document.querySelector('#playground .html'), {
   placeholder: '<div class="welcome">\n  <h1>Hello World</h1>\n  <p>Enter your HTML here.</p>\n</div>',
   mode: 'htmlmixed'
});

var cssPlayground = CodeMirror.fromTextArea(document.querySelector('#playground .css'), {
   placeholder: '.welcome {\n  text-align: center;\n}',
   mode: 'css'
});

var jsPlayground = CodeMirror.fromTextArea(document.querySelector('#playground .js'), {
   placeholder: 'var $ = require("jQuery");',
   mode: 'javascript'
});

var ifrm = document.getElementById('preview-panel');
ifrm = ifrm.contentWindow || ifrm.contentDocument.document || ifrm.contentDocument;

var createCSS = function (css) {
   var e = document.createElement('style');
   e.innerHTML = css;
   return e;
};

var createJS = function (js) {
   var e = document.createElement('script');
   e.innerHTML = js;
   return e;
}

var update = function (cm) {
   ifrm.document.open();
   ifrm.document.write(htmlPlayground.getValue());
   ifrm.document.head.appendChild(createCSS(cssPlayground.getValue()));
   ifrm.document.body.appendChild(createJS(jsPlayground.getValue()));
   ifrm.document.close();
}

htmlPlayground.on('change', update);
cssPlayground.on('change', update);
jsPlayground.on('change', update);