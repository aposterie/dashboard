var getTypeOf = function (obj) {
   return Object.prototype.toString.call(obj).toLowerCase().slice(8, -1);
};

// CodeMirror
require('lib/codemirror/mode/javascript.min.js');
require('lib/codemirror/mode/css.min.js');
require('lib/codemirror/mode/xml.min.js');
require('lib/codemirror/mode/htmlmixed.js')

require('js/console.min.js', 'css/console.css');
require('js/playground.js', 'css/playground.css');
